import RPi.GPIO as GPIO
import time

PWM_pins = [18, 13]  #These are the GPIO output pins with LED connected
channel_map = {}

# =============================================================================
# create a class for pwm pins
# -----------------------------------------------------------------------------
class PiPwm(object):
    """Raspberry Pi Internet 'IoT GPIO PWM'."""

    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)              # BMC Pin numbering convention
        for PWM_pin in PWM_pins:            # For each pin listed, setup as output
            GPIO.setup(PWM_pin, GPIO.OUT)   # pin as output
            GPIO.output(PWM_pin,0)          # initialize state to off

    # start a PWM running
    def start_pwm(self, pin_num, freq, duty):
        """Setup and start a PWM to a particular frequency and duty cycle"""
        pwm_ch = GPIO.PWM(PWM_pins[pin_num], freq)
        channel_map[pwm_ch] = PWM_pins[pin_num]
        pwm_ch.start(duty)
        return pwm_ch

    # change a running PWM's duty cycle
    def change_duty_cycle(self, pwm_ch, duty):
        pwm_ch.ChangeDutyCycle(duty)

    # change a running PWM's duty cycle
    def change_frequency(self, pwm_ch, freq):
        pwm_ch.ChangeFrequency(freq)

    # stop a PWM running
    def stop_pwm(self, pwm_ch):
        """Stop a PWM on a particular channel"""
        pwm_ch.stop()
        #make sure output is off
        GPIO.setup(channel_map[pwm_ch], GPIO.OUT)
        GPIO.output(channel_map[pwm_ch], 0)

def demo():
    """Class self-demo function"""
    pipwm = PiPwm()
    PwmPulseDelay = 0.03
    chans = [
        {'freq1': 0.5, 'duty': 25, 'freq2': 20},
        {'freq1': 2, 'duty': 50, 'freq2': 30}
    ]

    #Start PWM on all available channels (max of 3)
    print("Starting with pins: ")
    for po in range(len(chans)):
        print("{}: {}Hz, {}% duty cycle".format(po, chans[po]['freq1'], chans[po]['duty']))
        chans[po]['pwm'] = pipwm.start_pwm(po, chans[po]['freq1'], chans[po]['duty'])
    time.sleep(10.0)

    #Then update the PWM operating frequency to freq2
    print("Updating PWM frequencies:")
    for po in range(len(chans)):
        print("{}: {}Hz".format(po, chans[po]['freq2']))
        pipwm.change_frequency(chans[po]['pwm'], chans[po]['freq2'])
    time.sleep(10.0)

    #Then update the duty cycles to breath
    print("Changing the duty cycles, 0 - 100%")
    for tt in range(5):
        for dc in range(100, -1, -1):
            setAllPwm(pipwm, chans, dc)
            time.sleep(PwmPulseDelay)
        for dc in range(0, 101, 1):
            setAllPwm(pipwm, chans, dc)
            time.sleep(PwmPulseDelay)
    time.sleep(5.0)

    #Done with demo, turn off outputs
    print("Stopping all PWM outputs")
    for po in range(len(chans)):
        pipwm.stop_pwm(chans[po]['pwm'])

def setAllPwm(pipwm, pwm_outputs, newDutyCycle):
    for po in range(len(pwm_outputs)):
        pipwm.change_duty_cycle(pwm_outputs[po]['pwm'], newDutyCycle)

if __name__ == "__main__":
    demo()

