$(document).ready(function() {
     // establish global variables for LED status
     var led1;
     var led2;
     var led3;

    var cached_sens_reads = [];
    var temp_data = [];
    var tempGraph = new Morris.Line({
        element: 'mytempchart',
        data: [],
        xkey: 'time',
        ykeys: ['temp'],
        labels: ['Temperature (C)']
    });

    var pressGraph = new Morris.Line({
        element: 'mypresschart',
        data: [],
        xkey: 'time',
        ykeys: ['press'],
        labels: ['Pressure (hPa)']
    });

    iotSource.onmessage = function(e) {
        sse_data = JSON.parse(e.data);
        console.log(sse_data);
        //var params = e.data.split(' ');
        updateSwitch(sse_data["switch"]);
        updateLeds(1, sse_data["led_red"]);
        updateLeds(2, sse_data["led_grn"]);
        updateLeds(3, sse_data["led_blu"]);
        cached_sens_reads.unshift({
            'time': Date.parse(sse_data['meas_time']),
            'temp': sse_data['temperature']['reading'],
            'press': sse_data['pressure']['reading']
        });

        while (cached_sens_reads.length > 20) cached_sens_reads.pop();
        updateDataTable();
        updateCharts();
    }

    //update data table
    function updateDataTable() {
        rStr = "";
        cached_sens_reads.slice(0, 5).forEach(function(di) {
            rStr += "<tr>";
            rStr += "<td>" + di['time'] + "</td>";
            rStr += "<td>" + di['temp'] + "</td>";
            rStr += "<td>" + di['press'].toFixed(4) + "</td>";
            rStr += "</tr>";
            //console.log(di);
        });
        $("tbody#sensor-data").html(rStr);
    }

    //function to update temperature chart
    function updateCharts(){

        tempGraph.setData(cached_sens_reads);
        pressGraph.setData(cached_sens_reads);
    }


     /* update the Switch based on its SSE state monitor */
        function updateSwitch(switchValue) {
          if (switchValue === '1') {
	    //$('#switch').text(9759);
            $('#switch').toggleClass('label-default', false);
	    $('#switch').toggleClass('label-success', true);
	    $('#switch').text('ON');
          } else if (switchValue === '0') {
	    // $('#switch').text(OFF)
            $('#switch').toggleClass('label-default', true);
	    $('#switch').toggleClass('label-success', false);
	    $('#switch').text('OFF')
          }
        }

        /* update the LEDs based on their SSE state monitor */
        function updateLeds(ledNum,ledValue) {
	  //RED LED
          if (ledNum === 1) {
            if (ledValue === '1') {
              $('#red_led_label').toggleClass( 'label-default', false);
	      $('#red_led_label').toggleClass( 'label-danger', true);
              led1 = "ON"
            } else if (ledValue === '0') {
              $('#red_led_label').toggleClass( 'label-default', true);
      	      $('#red_led_label').toggleClass( 'label-danger', false);
              led1 = "OFF"
            }
          }
	  //GREEN LED
          else if (ledNum === 2) {
            if (ledValue === '1') {
              $('#grn_led_label').toggleClass( 'label-default', false);
      	      $('#grn_led_label').toggleClass( 'label-danger', true);
              led2 = "ON"
            } else if (ledValue === '0') {
              $('#grn_led_label').toggleClass( 'label-default', true);
       	      $('#grn_led_label').toggleClass( 'label-danger', false);
              led2 = "OFF"
            }
          }
	  //BLUE LED
	  else if (ledNum === 3) {
            if (ledValue === '1') {
              $('#blu_led_label').toggleClass( 'label-default', false);
      	      $('#blu_led_label').toggleClass( 'label-danger', true);
              led3 = "ON"
            } else if (ledValue === '0') {
              $('#blu_led_label').toggleClass( 'label-default', true);
      	      $('#blu_led_label').toggleClass( 'label-danger', false);
              led3 = "OFF"
            }
          }
       }


     // The button click functions run asynchronously in the browser
     $('#red_led_btn').click(function() {
        var params = 'led=1&state='+led1;
        console.log('Led Toggle with params:' + params);
        $.post('/ledtoggle', params, function(data, status){
              console.log("Data: " + data + "\nStatus: " + status);
              $.get('/leds/1', function(data) {
                   led1 = $.trim(data.split(':')[1]);
                   //if (led1 === '0') {led1 = "OFF";} else {led1 = "ON";}
                   //$('#led1_state').text(led1);
              });
        });
      });

      // The button click functions run asynchronously in the browser
      $('#grn_led_btn').click(function() {
            if (led2 === "OFF") {led2 = "ON";} else {led2 = "OFF";}
            var params = 'led=2&state='+led2;
            console.log('Led Command with params:' + params);
            $.post('/ledcmd', params, function(data, status){
                    console.log("Data: " + data + "\nStatus: " + status);
                    //$('#led2_state').text(led2);
            });
      });

      // The button click functions run asynchronously in the browser
      $('#blu_led_btn').click(function() {
            if (led3 === "OFF") {led3 = "ON";} else {led3 = "OFF";}
            var params = 'led=3&state='+led3;
            console.log('Led Command with params:' + params);
            $.post('/ledcmd', params, function(data, status){
                    console.log("Data: " + data + "\nStatus: " + status);
                    //$('#led3_state').text(led3);
            });
      });
   });

