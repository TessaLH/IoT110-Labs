#Tessa Lab 1
#Hello.py

from flask import Flask
import socket

# Get my machine hostname
if socket.gethostname().find('.') >= 0:
	hostname=socket.gethostname()
else:
	hostname=socket.gethostbyaddr(socket.gethostname())[0]

app = Flask(__name__)

@app.route("/")
def hello():
	return "\nHello IoT World from RPi3: " + hostname + "!\n\n"

# Run the website and make sure to make it extremely visible 
# with 0.0.0.0:5000 (default port)
if __name__ == "__main__":
	app.run(host='0.0.0.0')

